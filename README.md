Jigsaw Creator
-------------
A small app that lets you create a jigsaw puzzle out of an image.


The goal of this app is to digitally export the puzzles for a later puzzle solver app.

However it seems like it might be fun as its own app.


Building
---
This project only has javascript dependencies, so with a recent node version do

```javascript
npm install
npm run dev
```
To run the project.
